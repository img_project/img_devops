CURRENT_DIR=$(shell pwd)

auth:
	mkdir -p ${CURRENT_DIR}/ssh/
	ssh-keygen -q -t rsa -C 'docker' -N '' -f ${CURRENT_DIR}/ssh/id_rsa <<y >/dev/null 2>&1

up:
	docker-compose up --build -d

down:
	docker-compose down --rmi local

log:
	docker-compose logs -f

.PHONY: up down

.DEFAULT_GOAL:=up