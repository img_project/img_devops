### Img resizer project devops

To run this project:

1. Generate ssh key and add your generated public key to gitlab access keys list (for example ssh public key located into **ssh/id_rsa.pub**)
   ```bash
   make auth
   ```

2. Create database service
   ```bash
   docker-compose up -d postgres_service
   ```
3. Create all databases
   ```bash
   ./scripts/init/db.sh dbname
   ```
4. Build all services
   ```bash
   make up
   ```
5. Downing all services
   ```
   make down
   ```

---

Necessary .env file examples located into each service own directory.

For creating user run following command
```bash
./scripts/init/create_user.sh --username myuser --password mysecretpassword
```

For more information
```bash
./scripts/init/create_user.sh --help
```

For simple stress testing
```bash
./scripts/test/stress.sh $NUMBER_OF_REQUESTS_PER_SECOND http://$HOST:$PORT/api/v1/list $ACCESS_TOKEN
```