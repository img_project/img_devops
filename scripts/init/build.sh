#!/bin/bash

RUN_MIGRATION=$1
DIR=$2
INPUT_PATH=$3
OUTPUT_PATH=${DIR}/
CREATE_USER=$4

function gen_proto() {
    rm -rf ${OUTPUT_PATH}/protobuf 

    for x in $(find ${DIR}/${INPUT_PATH}/* -type d); do
    if ls ${x}/*.proto &>/dev/null
    then
        protoc -I=${x} -I=${DIR}/${INPUT_PATH} --go_out=plugins=grpc:${OUTPUT_PATH} ${x}/*.proto
    fi
    done
}

if [ "$RUN_MIGRATION" == "true" ]
then
    echo "Migration started..."
    make migrate-up
fi
echo "Generate proto files..."
gen_proto
echo "Make vendor..."
make vendor
echo "Build..."
go build -o app $DIR/cmd/
if [ "$CREATE_USER" == "true" ]
then
    echo "user create script generating..."
    go build -o create $DIR/cmd/script/
fi
echo "Start..."
./app
